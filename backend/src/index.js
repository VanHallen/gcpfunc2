// bring in Feathers API
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');

// bring in application specific elements
const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');
const mongoose = require('./mongoose');

// construct Feathers app
const app = feathers();

// Load app configuration
app.configure(configuration());

// Load mongoose configuration
app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);

// Set up our services (see `services/index.js`)
app.configure(services);

// Set up event channels (see channels.js)
app.configure(channels);

// register application level hooks
app.hooks(appHooks);

// Set up Google Cloud Functions entry point
exports.feathers = async (req, res) => {
    // for CORS
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', '*');
    if ( req.method === 'OPTIONS') {
      // Send response to OPTIONS requests for CORS
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.status(204).send('');
  
    } else if ( req.method === 'POST' && req.path === '/books') {
      const books = app.service('books');
      const result = await books.create( req.body );
      res.json( result );
    } else if ( req.method === 'GET' && req.path === '/books') {
        const books = app.service('books');
        const result = await books.find();
        res.json( result );
    } else if ( req.method === 'DELETE' && req.path.match(/\/books\/[a-z0-9]{24}/)) {
        const books = app.service('books');
        const result = await books.remove(req.path.split("/")[2]);
        res.json( result );
    } else {
      console.log( "method: " + req.method );
      console.log( "path: " + req.path );
      res.send('Unsupported request');
    }
  };
