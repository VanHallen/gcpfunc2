import React, { Component } from 'react';
import client from './feathers';

class Books extends Component {
   constructor(props) {
    super(props);
    
    this.state = {};
  }

  componentDidMount() {
    const bookService = client.service('books');
    bookService.find({
        query: {
          $limit: 25
        }
    }).then( allBooks => {
        const books = allBooks.data;
        this.setState({ bookService, books });
    });

    // Remove a book from the list
    bookService.on('removed', book => {
        console.log("removed id: " + book._id );
        const newbooks = this.state.books.filter((deletedBook,index,arr) => {
          return deletedBook._id !== book._id;
        });
        this.setState({ 
          books: newbooks
        });
    });

    // Add new book to the book list
    bookService.on('created', book => this.setState({
        books: this.state.books.concat(book)
      }));
  }

  addBook(ev) {
    
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = parseInt( inputISBN.value.trim() );

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = parseInt( inputPages.value.trim() );

    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );

    this.state.bookService.create({
        isbn,
        title,
        pages
    })
    .then(() => {
        inputISBN.value = '';
        inputTitle.value = '';
        inputPages.value = '';
    });
    
    ev.preventDefault();
  }

  deleteBook(id, ev) {
    this.state.bookService.remove( id );
  }

  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>

      <table id="books-list" className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
            {this.state.books && this.state.books.map(book => <tr key={book._id}>
                <th scope="row">{book.isbn}</th>
                <td>{book.title}</td>
                <td>{book.pages}</td>
                <td><button onClick={this.deleteBook.bind(this, book._id)} type="button" className="btn btn-danger">Delete</button></td>
            </tr>)}
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
